<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  login_model
 */
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->model('login_model');
        $this->load->database('default');
        $this->load->library('session');
    }

    /**
     * Load the view login_view
     */
    public function index()
    {
        $this->load->view('login_view');
    }

    /**
     * Validates the user and password
     *
     * @Params
     *
     * @Post
     * username
     * password
     *
     * @Return
     * Redirect to Accounts controller if success
     */
    public function validLogin()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $account = $this->login_model->validLogin($username, $password);
        if ($account != null) {
            $session = array(
                'idAccount' => $account['id']
            );
            $this->session->set_userdata($session);
            redirect(base_url() . 'Accounts');
        } else {
            redirect(base_url());
        }
    }

    /**
     * Create a new user acount
     *
     * @Params
     *
     * @Post
     * username
     * name
     * mail
     * password
     * city
     * address
     * state
     * zip_code
     * phone_number
     *
     * @Return
     *
     */
    public function newUser()
    {
        $username = $_POST['username'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        $city = $_POST['city'];
        $address = $_POST['address'];
        $state = $_POST['state'];
        $zip_code = $_POST['zip_code'];
        $phone_number = $_POST['phone_number'];
        $this->login_model->addUser($username, $name, $email, $phone_number, $password, $state, $city, $address, $zip_code);
    }
}
