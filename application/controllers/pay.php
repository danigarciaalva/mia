<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('openpay/Openpay');
        $this->load->database('default');
        $this->load->model('account_model');
        $this->load->model('subscription_model');
        $this->load->library('session');

        if (isset($this->openpay)) {
            $this->openpay->setProductionMode(false);
        }
    }

    /**
     * Load de pay view and get the subscription
     *
     * @Params
     *
     * @Post
     * checkbox
     * password
     *
     * @Return
     * $data
     *
     * Redirect to pay_view
     */
    public function index()
    {
        $members = $_POST["checkbox"];
        $openpay = $this->openpay->getInstance('ml8sfxccerwza3kpje7r', 'sk_7ec1ebc617564ea0ac94f28ab5cea884');
        if (count($members) > 1) {
            $subscription = $this->subscription_model->findMainSubscriptionWithNumberOfMembers(count($members) - 1);
        } else {
            $subscription = $this->subscription_model->findMainSubscriptionWithCode("pa0bs31ik2vy9fhdvhhe");
        }
        if ($subscription != null) {
            $plan = $openpay->plans->get($subscription["code"]);
            $data = array(
                'plan' => $plan,
                'members' => $members
            );
            $this->load->view('pay_view', $data);
        }
    }

    /**
     * Executes the payment order
     *
     * @Params
     *
     * @Post
     * checkbox
     * plan_id
     * nameT
     * NoT
     * cvv2
     * expiration_month
     * expiration_year
     *
     * @Return
     * $data
     *
     * Redirect to Accounts controller
     */
    public function pay()
    {
        $members = $_POST["checkbox"];
        $plan_id = $_POST["plan_id"];
        $holder_name = $_POST["nameT"];
        $card_number = $_POST["NoT"];
        $cvv2 = $_POST["cvv2"];
        $expiration_month = $_POST["expiration_month"];
        $expiration_year = $_POST["expiration_year"];

        /** initialize the object openpay with the credentials @Params ID, Llave privada */
        $openpay = $this->openpay->getInstance('ml8sfxccerwza3kpje7r', 'sk_7ec1ebc617564ea0ac94f28ab5cea884');

        $idAccount = $this->session->userdata('idAccount');
        $account = $this->account_model->getAccount($idAccount);
        $anchor = $this->account_model->getDataMemberAnchor($idAccount);


        /** Create the address array for the customer */
        $address = array(
            'line1' => $account["address"],
            'line2' => '',
            'line3' => '',
            'postal_code' => $account["zip_code"],
            'state' => $account["state"],
            'city' => $account["city"],
            'country_code' => 'MX');

        /** Create the customer array, adding the address array as param */
        $customerData = array(
            'name' => $anchor["name"],
            'last_name' => "",
            'email' => $account["email"],
            'address' => $address,
            'phone_number' => $account["phone_number"]
        );

        /** Crate the customer with the $customerData */
        $customer = $openpay->customers->add($customerData);

        /** Create de CardData array wiht the form values */
        $cardData = array(
            'holder_name' => $holder_name,
            'card_number' => $card_number,
            'cvv2' => $cvv2,
            'expiration_month' => $expiration_month,
            'expiration_year' => $expiration_year,
            'address' => $address
        );

        /** Creat the object card with the $cardData array  */
        $card = $customer->cards->add($cardData);


        /**  Call the account_model to update the Account */
        $this->account_model->updateAccount($idAccount, $customer->id, $card->id);

        /** Creat the subscription with the plan_id and card_id */
        $subscriptionDataRequest = array(
            "trial_end_date" => "",
            'plan_id' => $plan_id,
            'card_id' => $card->id);

        /** Add the subscription the our customer */
        $subscription = $customer->subscriptions->add($subscriptionDataRequest);

        /** Call the account_model to update the member */
        $this->account_model->updateMembers($subscription->id, $members);
        redirect(base_url() . 'Accounts');
    }
    
}
