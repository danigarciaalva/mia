<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'form'));
        $this->load->library('openpay/Openpay');
        $this->load->model('account_model');
        $this->load->database('default');
        $this->load->library('session');

    }

    /**
     * Load the members and sub members from account
     *
     * @Params
     *
     * @Post
     *
     * @Return
     * $data
     *    $anchor
     *    $original
     *    $additionals
     *    $canSubscribeAdditionals
     *
     * Redirect to accounts_view
     */
    public function index()
    {
        $idAccount = $this->session->userdata('idAccount');
        $anchor = $this->account_model->getDataMemberAnchor($idAccount);

        $originals = null;
        if ($anchor['Subscription_id'] != "") {
            $originals = $this->account_model->getOriginals($idAccount, $anchor['Subscription_id']);
            $additionals = $this->account_model->getAdditionals($idAccount, $anchor['Subscription_id'], true);
            $canSubscribeAdditionals = true;
        } else {
            $additionals = $this->account_model->getAdditionals($idAccount, $anchor['Subscription_id'], false);
            $canSubscribeAdditionals = false;
        }
        $data = array(
            'anchor' => $anchor,
            'originals' => $originals,
            'additionals' => $additionals,
            'canSubscribeAdditionals' => $canSubscribeAdditionals
        );
        $this->load->view('accounts_view', $data);
    }

    /**
     * Add member to account
     *
     * @Params
     *
     * @Post
     * nameNewMember
     * relationshipNewMember
     *
     * @Return
     *
     * Redirect to Accounts controller
     */
    public function addNewMember()
    {
        $name = $_POST['nameNewMember'];
        $relationship = $_POST['relationshipNewMember'];
        $idAccount = $this->session->userdata('idAccount');
        $this->account_model->addNewMember($name, $relationship, $idAccount);
        redirect(base_url() . 'Accounts');
    }

    /**
     * Subscribe a new member to the plan
     *
     * @Params
     *
     * @Post
     * members
     *
     * @Return
     * $data
     *    $anchor
     *    $original
     *    $additionals
     *    $canSubscribeAdditionals
     *
     * Redirect to Accounts controller
     */
    public function subscribeAdditional()
    {
        $idAccount = $this->session->userdata('idAccount');
        $members = $_POST['members'];

        $account = $this->account_model->getAccount($idAccount);

        $openpay = $this->openpay->getInstance('ml8sfxccerwza3kpje7r', 'sk_7ec1ebc617564ea0ac94f28ab5cea884');
        $customer = $openpay->customers->get($account["open_pay_id"]);
        $subscriptionDataRequest = array(
            "trial_end_date" => "",
            'plan_id' => 'pxwkacj9q8ivqsmewfwz',
            'card_id' => $account["open_pay_token"]);

        $subscription = $customer->subscriptions->add($subscriptionDataRequest);
        $this->account_model->updateMembers($subscription->id, $members);
        redirect(base_url() . 'Accounts');
    }

    /**
     * Cancel the subscription
     *
     * @Params
     *
     * @Post
     * members
     *
     * @Return
     *
     * Redirect to Accounts controller
     */
    public function cancelSubscription()
    {
        $idAccount = $this->session->userdata('idAccount');
        $memberIds = $_POST['members'];
        foreach ($memberIds as $memberId) {
            $member = $this->account_model->getMember($memberId);

            $account = $this->account_model->getAccount($idAccount);

            $openpay = $this->openpay->getInstance('ml8sfxccerwza3kpje7r', 'sk_7ec1ebc617564ea0ac94f28ab5cea884');
            $customer = $openpay->customers->get($account["open_pay_id"]);

            $subscription = $customer->subscriptions->get($member['Subscription_id']);
            try {
                $subscription->delete();
            } catch (Exception $e) {

            }
        }

        $this->account_model->updateMembers("", $memberIds);
        redirect(base_url() . 'Accounts');
    }

    /**
     * Cancel the Anchor
     *
     * @Params
     *
     * @Post
     *
     * @Return

     */
    public function cancelAnchor()
    {
        $idAccount = $this->session->userdata('idAccount');

    }
}
