<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('uuid.php');
        $this->load->helper('date');
    }

    /**
     * Get the data Account from Account table
     *
     * @Params
     * $idAccount
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     */
    public function getAccount($idAccount)
    {
        $this->db->select("id, username, email, open_pay_id, open_pay_token, address, phone_number, zip_code, city, state");
        $this->db->where("id", $idAccount);
        $query = $this->db->get("Account");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    /**
     * Get the members from the account on Member table
     *
     * @Params
     * $idMember
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     */
    public function getMember($idMember)
    {
        $this->db->select();
        $this->db->where("id", $idMember);
        $query = $this->db->get("Member");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    /**
     * Get the Anchor from Account on Member table
     *
     * @Params
     * $idAccount
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     */
    public function getDataMemberAnchor($idAccount)
    {
        $this->db->select();
        $this->db->where("Account_id", $idAccount);
        $this->db->where("is_anchor", 1);
        $query = $this->db->get("Member");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    /**
     * Get the members from the first subscription on Member table
     *
     * @Params
     * $idAccount
     * $subscriptionId
     *
     * @Post
     *
     * @Return
     * Query result
     *
     */
    public function getOriginals($idAccount, $subscriptionId)
    {
        $this->db->select();
        $this->db->where("Account_id", $idAccount);
        $this->db->where("Subscription_id", $subscriptionId);
        $this->db->where("is_anchor", 0);
        return $this->db->get("Member");
    }

    /**
     * Get the additional members from subscription on Member table
     *
     * @Params
     * $idAccount
     * $subscriptionId
     * $subscribed
     *
     * @Post
     *
     * @Return
     * Query result
     *
     */
    public function getAdditionals($idAccount, $subscriptionId, $subscribed)
    {
        $this->db->select();
        if ($subscribed == true) {
            $this->db->where("Subscription_id !=", $subscriptionId);
        }
        $this->db->where("Account_id", $idAccount);
        $this->db->where("is_anchor", 0);
        return $this->db->get("Member");
    }

    /**
     * Get the data from subscription on Subscription table
     *
     * @Params
     * $idAccount
     * $idMember
     *
     * @Post
     *
     * @Return
     * Query result
     *
     */
    public function getAnchorSubscription($idAccount, $idMember)
    {
        $this->db->select('Subscription_id');
        $this->db->where('id', $idMember);
        $this->db->where('is_anchor', 1);
        $this->db->where('Account_id', $idAccount);
        $query = $this->db->get('Member');
        $result = $query->result();
        if (count($result) > 0) {
            $subscriptionID = $result[0]->Subscription_id;
            $this->db->select();
            $this->db->where('id', $subscriptionID);
            $query = $this->db->get('Subscription');
            $result = $query->result();
            return $result[0];
        } else {
            return null;
        }
    }

    /**
     * Add a new member to the account on Member table
     *
     * @Params
     * $name
     * $raltionship
     * $idAccount
     *
     * @Post
     *
     * @Return
     *
     * Insert
     *
     */
    public function addNewMember($name, $relationship, $idAccount)
    {
        $uuidMember = $this->uuid->v4();
        $data = array(
            'id' => $uuidMember,
            'name' => $name,
            'relationship' => $relationship,
            'is_anchor' => 0,
            'Subscription_id' => "",
            'Account_id' => $idAccount,
            'image' => ""
        );

        $this->db->insert('Member', $data);
    }

    /**
     * Save the tokens from Openpay
     *
     * @Params
     * $idAccount
     * $openPayId
     * $openPayToken
     *
     * @Post
     *
     * @Return
     *
     * Update
     */
    public function updateAccount($idAccount, $openPayId, $openPayToken)
    {
        $accountData = array(
            'open_pay_id' => $openPayId,
            'open_pay_token' => $openPayToken,
        );
        $this->db->where('id', $idAccount);
        $this->db->update('Account', $accountData);
    }

    /**
     * Update the members from the account with the subscription code
     *
     * @Params
     * $subscriptionCode
     * $members
     *
     * @Post
     *
     * @Return
     *
     * Update
     */
    public function updateMembers($subscriptionCode, $members)
    {
        $data = array(
            'Subscription_id' => $subscriptionCode
        );
        foreach ($members as $member) {
            $this->db->where('id', $member);
            $this->db->update('Member', $data);
        }
    }

}