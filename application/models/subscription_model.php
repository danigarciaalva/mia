<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: danielgarcia
 * Date: 3/25/16
 * Time: 10:52 PM
 */
class Subscription_model extends CI_Model{

    function __construct()
    {
        parent::__construct();
        $this->load->library('uuid.php');
        $this->load->helper('date');
    }

    /**
     * Find subscription with the numbers of members acorded to the requiered
     *
     * @Params
     * $numbersOfMembers
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     * Return de data of the subscription
     */
    public function findMainSubscriptionWithNumberOfMembers($numberOfMembers)
    {
        $this->db->select();
        $this->db->where("additionals", $numberOfMembers);
        $this->db->where("used", false);
        $this->db->where("type", 1);
        $query = $this->db->get("Subscription");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    /**
     * Find subscription with the code
     *
     * @Params
     * $code
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     * Return de data of the subscription with the code $code
     */
    public function findMainSubscriptionWithCode($code){
        $this->db->select();
        $this->db->where("used", false);
        $this->db->where("type", 1);
        $this->db->where("code", $code);
        $query = $this->db->get("Subscription");
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return null;
        }
    }
}