
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pay_Model extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->load->library('uuid.php');
        $this->load->helper('date');
    }

    /**
     * Get the user data from Member table
     *
     * @Params
     * $idAccount
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     *
     * Return the user data
     */
    public function getUserData($idAccount){
        $this->db->select();
        $this->db->where("Account_idAccount", $idAccount);
        $this->db->where("is_anchor", 1);
        $query = $this->db->get("Member");
        return $query->row_array();
    }

    /**
     * Get the member data
     *
     * @Params
     * $idAccount
     *
     * @Post
     *
     * @Return
     * $query
     *
     *
     * Return the member data
     */
    public function getDataMember($idAccount){
        $this->db->select('Member.name, Member.relationship, Member.image, Subscription.active');
        $this->db->from('Member');
        $this->db->join('Subscription', 'Subscription.id = Member.Subscription_id', 'left');
        $this->db->where("Member.Account_idAccount", $idAccount);
        $this->db->where("Member.is_anchor", 0);
        $query = $this->db->get();
        return $query;
    }

    /**
     * Get the Anchor subscription
     *
     * @Params
     * $idAccount
     *
     * @Post
     *
     * @Return
     * $result
     *
     * Return active
     */
    public function getAnchorSubscription($idAccount){
        $this->db->select('Subscription_id');
        $this->db->where('Account_idAccount', $idAccount);
        $this->db->where('is_anchor', 1);
        $query = $this->db->get('Member');
        $result = $query->result();
        $subscriptionID = $result[0]->Subscription_id;

        $this->db->select('active');
        $this->db->where('id', $subscriptionID );
        $query = $this->db->get('Subscription');
        $result = $query->result();
        return $result[0]->active;

    }

    /**
     * Get subscription data from Subscription table
     *
     * @Params
     * $idAccount
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     */
    public function getDataSubscription($idAccount){
        $this->db->select();
        $this->db->where("Account_idAccount", $idAccount);
        $query = $this->db->get("Subscription");
        return $query->row_array();
    }

    /**
     * Add new member to account
     *
     * @Params
     * $name
     * $relationship
     * $idAccount
     *
     * @Post
     *
     * @Return
     * Insert
     */
    public function addNewMember($name, $relationship, $idAccount){

        $uuid = $idAccount;

        $uuidSuscription = $this->uuid->v4();
        $suscriptionData = array(
            'id' => $uuidSuscription,
            'initial_date' => time(),
            'active' => 0,
            'amount' => 18,
            'Account_idAccount' => $uuid,
            'is_main' => 1
        );
        $this->db->insert('Subscription', $suscriptionData);

        $uuidMember = $this->uuid->v4();
        $data = array(
            'id' => $uuidMember,
            'name' => $name,
            'relationship' => $relationship,
            'is_anchor' => 0,
            'Subscription_id' => $uuidSuscription,
            'Account_idAccount' => $uuid,
            'image' => ""
        );

        $this->db->insert('Member', $data);
    }
}