<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('uuid.php');
        $this->load->helper('date');
    }

    /**
     * Valid the login user and password
     *
     * @Params
     * $username
     * $password
     *
     * @Post
     *
     * @Return
     * $query->row_array()
     *
     * Return the user from query
     */
    public function validLogin($username, $password)
    {
        $this->db->select();
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('Account');
        if ($query->num_rows() == 1) {
            return $query->row_array();
        } else {
            return null;
        }
    }

    /**
     * Add a new user to the account
     *
     * @Params
     * $username
     * $name
     * $email
     * $phone_number
     * $password
     * $state
     * $city
     * $address
     * $zip_code
     *
     * @Post
     *
     * @Return
     *
     * Redirect to base_url
     */
    public function addUser($username, $name, $email, $phone_number, $password, $state, $city, $address, $zip_code)
    {
        $uuid = $this->uuid->v4();
        $userData = array(
            'id' => $uuid,
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'phone_number' => $phone_number,
            'state' => $state,
            'city' => $city,
            'address' => $address,
            'zip_code' => $zip_code
        );
        $this->db->insert('Account', $userData);

        $uuidMember = $this->uuid->v4();
        $data = array(
            'id' => $uuidMember,
            'name' => $name,
            'relationship' => "PRINCIPAL",
            'is_anchor' => 1,
            'image' => "",
            'Subscription_id' => "",
            'Account_id' => $uuid
        );

        $this->db->insert('Member', $data);

        redirect(base_url());
    }
}