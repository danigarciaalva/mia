<?php
defined('BASEPATH') OR exit('No direct script access allowed');?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Medica</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/bootstrap-material-design.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/ripples.min.css" rel="stylesheet"/>
    <link href="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.css" rel="stylesheet">


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">


    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

</head>
<body>

<div class="panel" style="padding: 80px;">
    <form name="newMember" id="newMember" id="newMember" action="<?=base_url()?>Accounts/addNewMember" method="post" >
        <legend>Nuevo Miembro</legend>
        <div class="form-group">
            <label for="inputEmail" class="col-md-2 control-label">Nombre</label>

            <div class="col-md-10">
                <input type="text" name="name" class="form-control" id="name" placeholder="Nombre">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="col-md-2 control-label">Parentesco</label>

            <div class="col-md-10">
                <input type="text" name="relationship" class="form-control" id="relationship" placeholder="Parentesco">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                <button type="submit" class="btn btn-primary" style="float: right;">Agregar</button>
            </div>
        </div>
</div>
</form>

</body>
</html>