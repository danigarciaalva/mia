<?php
defined('BASEPATH') OR exit('No direct script access allowed');?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Medica</title>

    <link href="<?php echo base_url(); ?>assets/css/project.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/base.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/base.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/project.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/table.css" rel="stylesheet"/>

    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/base.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/project.js"></script>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
    <script type='text/javascript'
            src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>

</head>

<body class="page-brand">
<header class="header header-transparent header-waterfall affix-top">
    <ul class="nav nav-list pull-right">
        <li class="dropdown margin-right">
            <a class="dropdown-toggle padding-left-no padding-right-no" data-toggle="dropdown">
                <span class="access-hide">John Smith</span>
                <span class="avatar avatar-sm"><img alt="alt text for John Smith avatar"
                                                    src="images/users/avatar-001.jpg"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a class="padding-right-lg waves-attach waves-effect" href="javascript:void(0)"><span
                            class="icon icon-lg margin-right">account_box</span>Profile Settings</a>
                </li>
                <li>
                    <a class="padding-right-lg waves-attach waves-effect" href="javascript:void(0)"><span
                            class="icon icon-lg margin-right">add_to_photos</span>Upload Photo</a>
                </li>
                <li>
                    <a class="padding-right-lg waves-attach waves-effect" href="page-login.html"><span
                            class="icon icon-lg margin-right">exit_to_app</span>Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</header>
<main class="content">
    <div class="content-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                    <h1 class="content-heading">Medica</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                <section class="content-inner margin-top-no">
                    <div class="card">
                        <div class="card-main">
                            <form action="<?= base_url() ?>Pay/pay" method="POST" id="payment-form">
                                <input type="hidden" name="token_id" id="token_id">
                                <div class="card-inner">
                                    <table class="tableForm">
                                        <tr>
                                            <td>
                                                <div>
                                                    <span class="card-heading">Tarjetas de crédito</span>
                                                </div>
                                                <div>
                                                    <img src="<?php echo base_url(); ?>assets/img/cards1.png">
                                                </div>

                                            </td>
                                            <td class="tdFormLess"></td>
                                            <td>
                                                <div>
                                                    <span class="card-heading">Tarjetas de débito</span>
                                                </div>
                                                <div>
                                                    <img
                                                        src="<?php echo base_url(); ?>assets/img/cards2.png">
                                                </div>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="card-inner">
                                    <div class="sctn-row">

                                        <div class="form-group form-group-label form-group-brand">
                                            <label class="floating-label" for="nameT">Nombre del
                                                titular</label><input
                                                class="form-control" type="text"
                                                id="nameT"
                                                name="nameT"
                                                placeholder="Como aparece en la tarjeta"
                                                autocomplete="off"
                                                data-openpay-card="holder_name">
                                        </div>

                                    </div>
                                    <div class="sctn-row">
                                        <table class="tableForm">
                                            <tr>
                                                <td>
                                                    <div class="form-group form-group-label form-group-brand">
                                                        <label class="floating-label" for="NoT">Número de
                                                            tarjeta</label><input type="text"
                                                                                  class="form-control"
                                                                                  autocomplete="off"
                                                                                  data-openpay-card="card_number"
                                                                                  value="4111111111111111"
                                                                                  id="NoT"
                                                                                  name="NoT"></div>

                                                </td>
                                                <td class="tdFormLess"></td>
                                                <td class="tdFormLabel">
                                                    <div class="form-group form-group-label form-group-brand">
                                                        <label class="floating-label" for="dateM">Mes de
                                                            expiración</label>
                                                        <div class="sctn-col half l">
                                                            <input class="form-control" type="text"
                                                                   placeholder="Mes"
                                                                   name="expiration_month"
                                                                   data-openpay-card="expiration_month">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="tdFormLess"></td>
                                                <td class="tdFormLabel">
                                                    <div class="form-group form-group-label form-group-brand">
                                                        <label class="floating-label" for="dateY">Año de
                                                            expiración</label>
                                                        <div class="sctn-col half l"><input class="form-control"
                                                                                            type="text"
                                                                                            placeholder="Año"
                                                                                            name="expiration_year"
                                                                                            data-openpay-card="expiration_year">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="tdFormLess"></td>
                                                <td>
                                                    <div class="form-group form-group-label form-group-brand">
                                                        <label class="floating-label">Código de
                                                            seguridad</label>
                                                        <div class="sctn-col half l"><input
                                                                class="form-control" type="text"
                                                                placeholder="3 dígitos"
                                                                autocomplete="off"
                                                                name="cvv2"
                                                                data-openpay-card="cvv2"
                                                                value=""></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="sctn-row">
                                        <table class="tableForm">
                                            <tr>
                                                <td class="tdFormCantidad">
                                                    <div class="form-group form-group-label form-group-brand">
                                                        <label class="floating-label">Cantidad</label>
                                                        <div class="sctn-col l">
                                                            <input class="form-control" value="<?=$plan->amount?>"
                                                                   name="amount" data-openpay-card="amount"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="tdForm"></td>
                                                <td>
                                                    <div class="form-group form-group-label form-group-brand">

                                                        <label class="floating-label">Concepto</label>
                                                        <div class="sctn-col l">
                                                            <input class="form-control"
                                                                   value="<?=$plan->name?>"
                                                                   name="description"
                                                                   descriptiondata-openpay-card="description"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr
                                        </table>


                                    </div>
                                    <table class="tableForm">
                                        <tr>
                                            <td class="tdForm">
                                                <div class="card-header">
                                                    <div class="card-inner">
                                                        <span>Transacciones realizadas vía:</span>
                                                    </div>
                                                    <div class="card-header-side">
                                                        <div>
                                                            <img
                                                                src="<?php echo base_url(); ?>assets/img/openpay.png">
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="tdForm"></td>
                                            <td>
                                                <div class="card-header">
                                                    <div class="card-inner">
                                                    <span>Tus pagos se realizan de forma segura con
                                                    encriptación
                                                    de 256 bits</span>
                                                    </div>
                                                    <div class="card-header-side">
                                                        <div>
                                                            <img
                                                                src="<?php echo base_url(); ?>assets/img/security.png">
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <div class="sctn-row">
                                        <button type="submit" class="btn btn-brand waves-attach waves-light waves-effect"
                                           style="float: right" id="pay-button">Pagar</button>
                                    </div>
                                    <br>
                                </div>
                                <?php foreach ($members as $member): ?>
                                    <input type="hidden" value="<?= $member ?>" name="checkbox[]"/>
                                <?php endforeach; ?>
                                <input type="hidden" name="plan_id" value="<?= $plan->id?>"/>
                            </form>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        </section>
    </div>
    </div>
    </div>

</main>
<footer class="ui-footer">
    <div class="container">
        <p>Medica</p>
    </div>
</footer>


<!-- js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/base.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/project.min.js"></script>
</body>


</html>