<?php
defined('BASEPATH') OR exit('No direct script access allowed');?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Medica</title>
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet"/>
</head>
<body>

<link
    href='http://fonts.googleapis.com/css?family=Roboto:900,900italic,500,400italic,100,700italic,300,700,500italic,100italic,300italic,400'
    rel='stylesheet' type='text/css'>

<div id="fback">
    <div class="girisback"></div>
    <div class="kayitback"></div>
</div>

<div id="textbox">
    <div class="toplam">

        <div class="left">
            <div id="ic">
                <h2>Crear Cuetna</h2>
                <p>Crea una nueva cuenta para MIA</p>
                <form id="girisyap" name="signup_form" id="signup_form" method="post"
                      action="<?= base_url() ?>Login/newUser" enctype="multipart/form-data">

                    <div class="yarim form-group">
                        <label class="control-label" for="inputNormal">Usuario</label>
                        <input type="text" name="username" id="signup_username"
                               class="bp-suggestions form-control" cols="50" rows="10">
                    </div>
                    <div class="yarim sn form-group">
                        <label class="control-label" for="inputNormal">Nombre</label>
                        <input type="text" name="name" id="field_1" value="" class="bp-suggestions form-control"
                               cols="50" rows="10">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputNormal">Correo</label>
                        <input type="text" name="email" id="signup_email" class="bp-suggestions form-control"
                               cols="50" rows="10">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputNormal">Contraseña</label>
                        <input type="password" name="password" id="signup_password" value=""
                               class="bp-suggestions form-control" cols="50" rows="10">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputNormal">Teléfono</label>
                        <input type="text" name="phone_number" id="phone_number" value=""
                               class="bp-suggestions form-control" cols="50" rows="10">
                    </div>

                    <div class="yarim form-group">
                        <label class="control-label" for="inputNormal">Estado</label>
                        <input type="text" name="state" id="state"
                               class="bp-suggestions form-control" cols="50" rows="10">
                    </div>
                    <div class="yarim sn form-group">
                        <label class="control-label" for="inputNormal">Ciudad</label>
                        <input type="text" name="city" id="city" value="" class="bp-suggestions form-control"
                               cols="50" rows="10">
                    </div>

                    <div class="yarim form-group">
                        <label class="control-label" for="inputNormal">Dirección</label>
                        <input type="text" name="address" id="address"
                               class="bp-suggestions form-control" cols="50" rows="10">
                    </div>
                    <div class="yarim sn form-group">
                        <label class="control-label" for="inputNormal">C.P.</label>
                        <input type="text" name="zip_code" id="zip_code" value="" class="bp-suggestions form-control"
                               cols="50" rows="10">
                    </div>


                    <input type="submit" name="signup_submit" id="signup_submit" value="Crear" class="girisbtn"/>
                </form>

                <button id="moveright">Iniciar Sesión</button>
            </div>
        </div>

        <div class="right">
            <div id="ic">
                <h2>Iniciar Sesión</h2>
                <p>Iniciar Sesión en MIA</p>

                <form name="login-form" id="girisyap" id="sidebar-user-login" action="<?= base_url() ?>Login/validLogin"
                      method="post">
                    <div class="form-group">
                        <label class="control-label" for="inputNormal">Usuario</label>
                        <input type="text" name="username" class="bp-suggestions form-control" cols="50" rows="10">
                    </div>
                    <div class="form-group soninpt">
                        <label class="control-label" for="inputNormal">Contraseña</label>
                        <input type="password" name="password" class="bp-suggestions form-control" cols="50"
                               rows="10">
                    </div>
                    <input type="submit" value="Login" class="girisbtn" tabindex="100"/>
                </form>

                <button id="moveleft">Nueva Cuenta</button>
            </div>
        </div>

    </div>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="<?php echo base_url(); ?>assets/js/index.js"></script>
</body>
</html>