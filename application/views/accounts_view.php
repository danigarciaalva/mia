<?php
defined('BASEPATH') OR exit('No direct script access allowed');?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Medica</title>

    <link href="<?php echo base_url(); ?>assets/css/project.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/base.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/base.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/css/project.min.css" rel="stylesheet"/>

    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/base.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/project.js"></script>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Material Design fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

</head>

<body class="page-brand">
<header class="header header-transparent header-waterfall affix-top">
    <ul class="nav nav-list pull-right">
        <li class="dropdown margin-right">
            <a class="dropdown-toggle padding-left-no padding-right-no" data-toggle="dropdown">
                <span class="access-hide">John Smith</span>
                <span class="avatar avatar-sm"><img alt="alt text for John Smith avatar"
                                                    src="images/users/avatar-001.jpg"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    <a class="padding-right-lg waves-attach waves-effect" href="javascript:void(0)"><span
                            class="icon icon-lg margin-right">account_box</span>Profile Settings</a>
                </li>
                <li>
                    <a class="padding-right-lg waves-attach waves-effect" href="javascript:void(0)"><span
                            class="icon icon-lg margin-right">add_to_photos</span>Upload Photo</a>
                </li>
                <li>
                    <a class="padding-right-lg waves-attach waves-effect" href="page-login.html"><span
                            class="icon icon-lg margin-right">exit_to_app</span>Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</header>
<main class="content">
    <div class="content-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                    <h1 class="content-heading">Medica</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                <section class="content-inner margin-top-no">
                    <div class="card">
                        <div class="card-main">
                            <div class="card-header">
                                <div class="card-header-side pull-left">
                                    <div class="avatar">
                                        <img alt="John Smith Avatar" src="images/users/avatar-001.jpg">
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <span class="card-heading"><?php echo $anchor['name'] ?></span>
                                </div>
                            </div>
                            <form method="post" action="<?= base_url() ?>Accounts/cancelSubscription">
                                <input type="hidden" name="members[]" value="<?= $anchor['id'] ?>"/>
                                <?php if ($originals != null && count($originals->result()) > 0) { ?>
                                    <div class="card-inner">

                                        <?php foreach ($originals->result() as $row) { ?>

                                            <div class="card-header">
                                                <div class="card-header-side pull-left">
                                                    <div class="avatar">
                                                        <img alt="John Smith Avatar"
                                                             src="images/users/avatar-001.jpg"/>
                                                    </div>
                                                </div>
                                                <div class="card-inner" style="white-space: nowrap;">
                                                <span class="card-heading"
                                                      style="float: left; display: block;"><?php echo $row->name ?> </span>
                                                    <code
                                                        style="margin-left: 15px; color: #3f51b5"><?php echo $row->relationship ?></code>
                                                </div>
                                            </div>

                                        <?php } ?>
                                    </div>
                                    <input type="hidden" name="members[]" value="<?= $row->id ?>"/>
                                <?php } ?>

                                <div class="card-action">
                                    <div class="card-action-btn pull-left">
                                        <?php if ($anchor['Subscription_id'] != "") { ?>

                                            <button type="submit"
                                                    class="btn btn-flat btn-red waves-attach waves-effect">CANCELAR
                                                SUSCRIPCIÓN
                                            </button>
                                        <?php } else { ?>
                                            <a class="btn btn-flat btn-green waves-attach waves-effect"
                                               data-toggle="modal"
                                               data-target="#createSubscription">SUSCRIBIR</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <?php if (count($additionals->result()) > 0) {

                        foreach ($additionals->result() as $row) { ?>
                            <div class="card">
                                <div class="card-main">
                                    <div class="card-header">
                                        <div class="card-header-side pull-left">
                                            <div class="avatar">
                                                <img alt="John Smith Avatar" src="images/users/avatar-001.jpg">
                                            </div>
                                        </div>
                                        <div class="card-inner" style="white-space: nowrap;">
                                            <span class="card-heading"
                                                  style="float: left; display: block;"><?php echo $row->name ?> </span>
                                            <code
                                                style="margin-left: 15px; color: #3f51b5"><?php echo $row->relationship ?></code>
                                        </div>
                                    </div>
                                    <?php if ($canSubscribeAdditionals) { ?>

                                        <div class="card-action">
                                            <div class="card-action-btn pull-left">

                                                <?php if ($row->Subscription_id == "") { ?>
                                                    <form method="post"
                                                          action="<?= base_url() ?>Accounts/subscribeAdditional">
                                                        <button type="submit"
                                                                class="btn btn-flat btn-green waves-attach waves-effect"><span
                                                                class="icon">check</span>&nbsp;Suscribir
                                                        </button>
                                                        <input type="hidden" name="members[]" value="<?= $row->id ?>"/>
                                                    </form>
                                                <?php } else { ?>
                                                    <form method="post"
                                                          action="<?= base_url() ?>Accounts/cancelSubscription">
                                                        <button type="submit"
                                                                class="btn btn-flat btn-red waves-attach waves-effect"
                                                                href=""><span
                                                                class="icon">check</span>&nbsp;Cancelar
                                                        </button>
                                                        <input type="hidden" name="members[]" value="<?= $row->id ?>"/>
                                                    </form>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        <?php }

                    } ?>

                </section>
            </div>
        </div>
    </div>
</main>
<footer class="ui-footer">
    <div class="container">
        <p>Medica</p>
    </div>
</footer>

<div class="fbtn-container">
    <div class="fbtn-inner">
        <a class="fbtn fbtn-lg fbtn-brand-accent waves-attach waves-circle waves-light waves-effect" data-toggle="modal"
           data-target="#addMemberModal">
            <span class="fbtn-ori icon">add</span>
        </a>
    </div>

</div>

<!-- Modal -->
<div aria-hidden="true" class="modal modal-va-middle fade" id="addMemberModal" role="dialog" tabindex="-1"
     style="display: none;">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="card-img">
                <img alt="alt text" src="<?php echo base_url(); ?>assets/img/formModal.jpg" style="width: 100%;">
            </div>
            <form class="modal-inner" name="newMember" id="newMember" id="newMember"
                  action="<?= base_url() ?>Accounts/addNewMember" method="post">
                <div class="form-group form-group-label form-group-brand">
                    <label class="floating-label" for="nameNewMember">Nombre</label>
                    <input class="form-control" name="nameNewMember" id="nameNewMember" type="text">
                </div>

                <div class="form-group form-group-label form-group-brand">
                    <label class="floating-label" for="relationshipNewMember">Relación</label>
                    <input class="form-control" name="relationshipNewMember" id="relationshipNewMember" type="text">
                </div>
                <div class="card-inner">
                    <p class="margin-top-no">
                        <a class="btn btn-flat btn-red waves-attach waves-effect" data-dismiss="modal">Cancelar</a>
                        &nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-brand waves-attach waves-light" style="float: right;">
                            Agregar
                        </button>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>

<div aria-hidden="true" class="modal modal-va-middle fade" id="createSubscription" role="dialog" tabindex="-1"
     style="display: none;">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="card-img">
                <img alt="alt text" src="<?php echo base_url(); ?>assets/img/suscribe.jpg" style="width: 100%;">
            </div>
            <form class="modal-inner" name="susbcribeMembers" id="susbcribeMembers" id="susbcribeMembers"
                  action="<?= base_url() ?>Pay" method="post">
                <div class="card-main">
                    <div class="card-inner">
                        <div class="form-group">

                            <div class="checkbox checkbox-adv checkbox-inline disabled">
                                <label for="doc_checkbox_example_2_disabled">
                                    <input class="access-hide" disabled="" id="doc_checkbox_example_2_disabled"
                                           name="checkbox[]" type="checkbox"
                                           checked=""><?php echo $anchor['name'] ?>
                                    <input type="hidden" value="<?php echo $anchor['id'] ?>"
                                           name="checkbox[]">
                                    <span class="checkbox-circle"></span><span
                                        class="checkbox-circle-check"></span><span
                                        class="checkbox-circle-icon icon">done</span>
                                </label>
                            </div>


                            <?php if ($additionals != null && count($additionals->result()) > 0) {

                                foreach ($additionals->result() as $row) { ?>

                                    <div class="checkbox checkbox-adv">
                                        <label for="<?php echo $row->name ?>">
                                            <input checked="" class="access-hide" id="<?php echo $row->name ?>"
                                                   name="checkbox[]"
                                                   value="<?php echo $row->id ?>"
                                                   type="checkbox"><?php echo $row->name ?>
                                            <span class="checkbox-circle"></span><span
                                                class="checkbox-circle-check"></span><span
                                                class="checkbox-circle-icon icon">done</span>
                                        </label>
                                    </div>

                                <?php }
                            }
                            ?>

                        </div>
                    </div>
                    <div class="card-inner">
                        <p class="margin-top-no">
                            <a class="btn btn-flat btn-red waves-attach waves-effect" data-dismiss="modal">Cancelar</a>
                            &nbsp;&nbsp;&nbsp;
                            <button type="submit" class="btn btn-brand waves-attach waves-light" style="float: right;">
                                Suscribir
                            </button>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/base.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/project.min.js"></script>
</body>


</html>