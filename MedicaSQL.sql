-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-03-2016 a las 08:02:05
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Medica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Account`
--

CREATE TABLE `Account` (
  `id` varchar(36) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `state` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip_code` int(10) NOT NULL,
  `open_pay_id` varchar(45) DEFAULT NULL,
  `open_pay_token` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Account`
--

INSERT INTO `Account` (`id`, `username`, `password`, `email`, `phone_number`, `state`, `city`, `address`, `zip_code`, `open_pay_id`, `open_pay_token`) VALUES
('e9944275-4d2a-4127-9b33-dd399325245b', 'christian', '123456', 'lirical_niggar@hotmail.com', '', 'Veracruz', 'Xalapa', 'Apeninos 8', 91180, NULL, NULL),
('f6a2475d-e5b5-404f-bd9d-4f3872b6ce59', 'liz', '123456', 'lirical_niggar@hotmail.com', '123456', 'Veracruz', 'Xalapa', 'Apeninos 8', 91180, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Member`
--

CREATE TABLE `Member` (
  `id` varchar(36) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `relationship` varchar(45) DEFAULT NULL,
  `is_anchor` tinyint(1) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `Subscription_id` varchar(45) DEFAULT NULL,
  `Account_id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Member`
--

INSERT INTO `Member` (`id`, `name`, `relationship`, `is_anchor`, `image`, `Subscription_id`, `Account_id`) VALUES
('2d8bd6cd-03aa-48e6-ad21-8f0e3307613b', 'Christian Vargas Saavedra', 'PRINCIPAL', 1, ' ', ' ', 'e9944275-4d2a-4127-9b33-dd399325245b'),
('5c9075a3-3fbe-40cb-bd6f-7e61c708b639', 'Christian', ':)', 0, '', '', 'f6a2475d-e5b5-404f-bd9d-4f3872b6ce59'),
('e8403a74-53e6-4ae9-bc2a-4a1b68cb0aa6', 'Lizeth Rodriguez', 'PRINCIPAL', 1, ' ', ' ', 'f6a2475d-e5b5-404f-bd9d-4f3872b6ce59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Subscription`
--

CREATE TABLE `Subscription` (
  `id` varchar(36) NOT NULL,
  `used` tinyint(1) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `additionals` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Account`
--
ALTER TABLE `Account`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Member`
--
ALTER TABLE `Member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Member_Account1_idx` (`Account_id`);

--
-- Indices de la tabla `Subscription`
--
ALTER TABLE `Subscription`
  ADD PRIMARY KEY (`id`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Member`
--
ALTER TABLE `Member`
  ADD CONSTRAINT `fk_Member_Account1` FOREIGN KEY (`Account_id`) REFERENCES `Account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
